package com.hxx.hospitalregistered.mapper;

import com.hxx.hospitalregistered.model.Information;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 咨询表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
public interface InformationMapper extends BaseMapper<Information> {

}
