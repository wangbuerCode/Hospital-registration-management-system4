package com.hxx.hospitalregistered.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseOBJ<T> {

    /**
     * 成功状态码
     */
    public final static Integer SUCCESS = 200;
    /**
     * 失败状态码
     */
    public final static Integer ERROR = -1;

    /**
     * layui统一返回格式
     */
    private Integer code;
    private String msg;
    private T data;
    private long count;

    public static ResponseOBJ getInstance() {
        return new ResponseOBJ<>();
    }
}
