package com.hxx.hospitalregistered.service;

import com.hxx.hospitalregistered.model.Patient;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 患者表 服务类
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
public interface PatientService extends IService<Patient> {

}
