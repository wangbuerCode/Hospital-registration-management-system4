package com.hxx.hospitalregistered.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hxx.hospitalregistered.model.Doctor;
import com.hxx.hospitalregistered.model.Record;
import com.hxx.hospitalregistered.service.RecordService;
import com.hxx.hospitalregistered.utils.ResponseOBJ;
import com.hxx.hospitalregistered.utils.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 病历 前端控制器
 * </p>
 *
 * @author 
 * @since 2020-031
 */
@RestController
@RequestMapping("/record")
public class RecordController {

    /**
     * 病历医生写，医生和患者都可以看（这是需求）
     */

    @Autowired
    RecordService recordService;

    /**
     * patientNo:患者编号下拉框选   doctorNo:session中取
     *
     * @param record
     * @return
     */
    @PostMapping("/addRecord")
    public ResponseOBJ<Boolean> addRecord(@RequestBody Record record) {
        ResponseOBJ<Boolean> responseOBJ = ResponseOBJ.getInstance();
        Doctor doctor = (Doctor) WebUtil.getSession().getAttribute("doctor");
        record.setDoctorNo(doctor.getId());
        record.setRecTime(new Date());
        boolean save = recordService.save(record);
        responseOBJ.setData(save);
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }


    @GetMapping("/listRecord")
    public ResponseOBJ<List<Record>> listRecord(@RequestParam("page") Integer page,
                                                @RequestParam("limit") Integer limit,
                                                @RequestParam(value = "patientNo", required = false) Integer patientNo) {
        ResponseOBJ<List<Record>> responseOBJ = ResponseOBJ.getInstance();
        if (patientNo != null) {
            Record record = new Record();
            record.setPatientNo(patientNo);
            PageHelper.startPage(page, limit);
            List<Record> list = recordService.list(new QueryWrapper<>(record));
            PageInfo<Record> pageInfo = new PageInfo<>(list);
            responseOBJ.setData(list);
            responseOBJ.setCount(pageInfo.getTotal());
            responseOBJ.setCode(ResponseOBJ.SUCCESS);
        } else {
            PageHelper.startPage(page, limit);
            List<Record> records = recordService.list();
            PageInfo<Record> pageInfo = new PageInfo<>(records);
            responseOBJ.setData(records);
            responseOBJ.setCount(pageInfo.getTotal());
            responseOBJ.setCode(ResponseOBJ.SUCCESS);
        }
        return responseOBJ;
    }
}

