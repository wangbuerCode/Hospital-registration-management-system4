package com.hxx.hospitalregistered.mapper;

import com.hxx.hospitalregistered.model.Message;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
public interface MessageMapper extends BaseMapper<Message> {

}
