package com.hxx.hospitalregistered.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hxx.hospitalregistered.model.Doctor;
import com.hxx.hospitalregistered.model.Message;
import com.hxx.hospitalregistered.model.Patient;
import com.hxx.hospitalregistered.service.MessageService;
import com.hxx.hospitalregistered.utils.ResponseOBJ;
import com.hxx.hospitalregistered.utils.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 留言 前端控制器
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@RestController
@RequestMapping("/message")
public class MessageController {

    @Autowired
    MessageService messageService;

    /**
     * 患者留言
     * doctorNo医生编号，弄个下拉框选
     */
    @PostMapping("/addMessage")
    public ResponseOBJ<Boolean> addMessage(@RequestBody Message message) {
        ResponseOBJ<Boolean> responseOBJ = ResponseOBJ.getInstance();
        Patient patient = (Patient) WebUtil.getSession().getAttribute("patient");
        message.setPatientNo(patient.getId());
        message.setMesTime(new Date());
        boolean save = messageService.save(message);
        responseOBJ.setData(save);
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }

    @GetMapping("/list")
    public ResponseOBJ<List<Message>> list(@RequestParam("page") Integer page,
                                           @RequestParam("limit") Integer limit) {
        ResponseOBJ<List<Message>> responseOBJ = ResponseOBJ.getInstance();
        PageHelper.startPage(page, limit);
        List<Message> messages = messageService.list();
        PageInfo<Message> pageInfo = new PageInfo<>(messages);
        responseOBJ.setData(messages);
        responseOBJ.setCount(pageInfo.getTotal());
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }


    /**
     * 查询当前登录医生的所有患者留言
     * @return
     */
    @GetMapping("/all")
    public ResponseOBJ<List<Message>> all(){
        Doctor doctor = (Doctor) WebUtil.getSession().getAttribute("doctor");
        Message message = new Message();
        message.setDoctorNo(doctor.getId());
        ResponseOBJ<List<Message>> responseOBJ = ResponseOBJ.getInstance();
        List<Message> messages = messageService.list(new QueryWrapper<Message>(message));
        responseOBJ.setData(messages);
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }
}

