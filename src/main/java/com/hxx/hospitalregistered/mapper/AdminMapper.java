package com.hxx.hospitalregistered.mapper;

import com.hxx.hospitalregistered.model.Admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 管理员表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
public interface AdminMapper extends BaseMapper<Admin> {

}
