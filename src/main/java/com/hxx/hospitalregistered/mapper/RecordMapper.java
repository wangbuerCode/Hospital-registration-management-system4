package com.hxx.hospitalregistered.mapper;

import com.hxx.hospitalregistered.model.Record;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 病例 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
public interface RecordMapper extends BaseMapper<Record> {

}
