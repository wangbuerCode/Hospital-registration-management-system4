package com.hxx.hospitalregistered.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hxx.hospitalregistered.model.Doctor;
import com.hxx.hospitalregistered.model.Patient;
import com.hxx.hospitalregistered.model.Registration;
import com.hxx.hospitalregistered.service.RegistrationService;
import com.hxx.hospitalregistered.utils.ResponseOBJ;
import com.hxx.hospitalregistered.utils.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Stream;

/**
 * <p>
 * 挂号表 前端控制器
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@RestController
@RequestMapping("/registration")
public class RegistrationController {

    @Autowired
    RegistrationService registrationService;

    /**
     * 挂号 doctorNo:弄个下拉框选医生，departmentNo弄个下拉框选科室
     */
    @PostMapping("/addRegistration")
    public ResponseOBJ<Boolean> addRegistration(@RequestBody Registration registration) {
        ResponseOBJ<Boolean> responseOBJ = ResponseOBJ.getInstance();
        registration.setRegTime(new Date());
        boolean save = registrationService.save(registration);
        responseOBJ.setData(save);
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }

    @GetMapping("/list")
    public ResponseOBJ<List<Registration>> list(@RequestParam("page") Integer page,
                                                @RequestParam("limit") Integer limit) {
        ResponseOBJ<List<Registration>> responseOBJ = ResponseOBJ.getInstance();
        PageHelper.startPage(page, limit);
        List<Registration> registrations = registrationService.list();
        PageInfo<Registration> pageInfo = new PageInfo<>(registrations);
        responseOBJ.setData(registrations);
        responseOBJ.setCount(pageInfo.getTotal());
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }


    /**
     * 获得当前需看诊的患者
     *
     * @return
     */
    @GetMapping("/getCurrentPatient")
    public ResponseOBJ<Registration> getCurrentPatient() {
        ResponseOBJ<Registration> responseOBJ = ResponseOBJ.getInstance();
        try {
            Doctor doctor = (Doctor) WebUtil.getSession().getAttribute("doctor");
            Registration registration = new Registration();
            registration.setDoctorNo(doctor.getId());
            PageHelper.startPage(1, 1);
            List<Registration> registrations = registrationService.list(new QueryWrapper<Registration>(registration));
            responseOBJ.setData(registrations.get(0));
            responseOBJ.setCode(ResponseOBJ.SUCCESS);
        } catch (Exception e) {
            responseOBJ.setCode(ResponseOBJ.ERROR);
        }

        return responseOBJ;
    }


    /**
     * 删除上一条挂号记录
     *
     * @param patId
     * @return
     */
    @GetMapping("/deleteBeforePatient")
    public ResponseOBJ<Boolean> deleteBeforePatient(@RequestParam("patId") Integer patId) {
        ResponseOBJ<Boolean> responseOBJ = ResponseOBJ.getInstance();
        responseOBJ.setData(registrationService.removeById(patId));
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }

    /**
     * 查看前面还有几位
     */
    @GetMapping("/previousNumber")
    public ResponseOBJ<Map<String,Object>> previousNumber() {
        ResponseOBJ<Map<String,Object>> responseOBJ = ResponseOBJ.getInstance();
        Map<String,Object> map = new HashMap<>();
        Patient patient = (Patient) WebUtil.getSession().getAttribute("patient");
        Registration reg = new Registration();
        reg.setPatName(patient.getPatName());
        Registration registration = registrationService.getOne(new QueryWrapper<Registration>(reg));
        List<Registration> lists = registrationService.list(new QueryWrapper<Registration>());
        List<Registration> arrayList = new ArrayList<>();
        for (Registration list : lists) {
            if (list.getPatName().equals(patient.getPatName())) {
                break;
            }
            arrayList.add(list);
        }
        map.put("registration", registration);
        map.put("count", arrayList.size());
        responseOBJ.setData(map);
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }

}

