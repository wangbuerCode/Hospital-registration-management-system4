package com.hxx.hospitalregistered.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hxx.hospitalregistered.model.Doctor;
import com.hxx.hospitalregistered.model.Patient;
import com.hxx.hospitalregistered.service.DoctorService;
import com.hxx.hospitalregistered.service.PatientService;
import com.hxx.hospitalregistered.utils.ResponseOBJ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 管理员表 前端控制器
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    PatientService patientService;

    @Autowired
    DoctorService doctorService;

    /**
     * 管理员添加患者
     *
     * @param patient
     * @return
     */
    @PostMapping("/addPatient")
    public ResponseOBJ<Boolean> addPatient(@RequestBody Patient patient) {
        ResponseOBJ<Boolean> responseOBJ = ResponseOBJ.getInstance();
        boolean save = patientService.save(patient);
        responseOBJ.setData(save);
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }

    /**
     * 管理员删除患者
     *
     * @param patient
     * @return
     */
    @PostMapping("/deletePatient")
    public ResponseOBJ<Boolean> deletePatient(@RequestParam("id") Integer id) {
        ResponseOBJ<Boolean> responseOBJ = ResponseOBJ.getInstance();
        boolean remove = patientService.removeById(id);
        responseOBJ.setData(remove);
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }

    /**
     * 管理员查询患者
     *
     * @param patient
     * @return
     */
    @GetMapping("/listPatient")
    public ResponseOBJ<List<Patient>> listPatient(@RequestParam("page") Integer page,
                                                  @RequestParam("limit") Integer limit) {
        ResponseOBJ<List<Patient>> responseOBJ = ResponseOBJ.getInstance();
        PageHelper.startPage(page, limit);
        List<Patient> patients = patientService.list();
        PageInfo<Patient> pageInfo = new PageInfo<>(patients);
        responseOBJ.setData(patients);
        responseOBJ.setCount(pageInfo.getTotal());
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }

    /**
     * 管理员添加医生
     *
     * @param patient
     * @return
     */
    @PostMapping("/addDoctor")
    public ResponseOBJ<Boolean> addDoctor(@RequestBody Doctor doctor) {
        ResponseOBJ<Boolean> responseOBJ = ResponseOBJ.getInstance();
        boolean save = doctorService.save(doctor);
        responseOBJ.setData(save);
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }

    /**
     * 管理员删除医生
     *
     * @param patient
     * @return
     */
    @PostMapping("/deleteDoctor")
    public ResponseOBJ<Boolean> deleteDoctor(@RequestParam("id") Integer id) {
        ResponseOBJ<Boolean> responseOBJ = ResponseOBJ.getInstance();
        boolean remove = doctorService.removeById(id);
        responseOBJ.setData(remove);
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }

    /**
     * 管理员查询医生
     *
     * @param
     * @return
     */
    @GetMapping("/listDoctor")
    public ResponseOBJ<List<Doctor>> listDoctor(@RequestParam("page") Integer page,
                                                @RequestParam("limit") Integer limit,
                                                @RequestParam(value = "docName", required = false) String docName) {
        ResponseOBJ<List<Doctor>> responseOBJ = ResponseOBJ.getInstance();
        if (docName != null) {
            Doctor doctor = new Doctor();
            doctor.setDocName(docName);
            PageHelper.startPage(page, limit);
            List<Doctor> doctors = doctorService.list(new QueryWrapper<Doctor>(doctor));
            PageInfo<Doctor> pageInfo = new PageInfo<>(doctors);
            responseOBJ.setData(doctors);
            responseOBJ.setCount(pageInfo.getTotal());
            responseOBJ.setCode(ResponseOBJ.SUCCESS);
        } else {
            PageHelper.startPage(page, limit);
            List<Doctor> doctors = doctorService.list();
            PageInfo<Doctor> pageInfo = new PageInfo<>(doctors);
            responseOBJ.setData(doctors);
            responseOBJ.setCount(pageInfo.getTotal());
            responseOBJ.setCode(ResponseOBJ.SUCCESS);
        }
        return responseOBJ;
    }
}

