package com.hxx.hospitalregistered.service;

import com.hxx.hospitalregistered.model.Information;
import com.hxx.hospitalregistered.mapper.InformationMapper;
import com.hxx.hospitalregistered.service.InformationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 咨询表 服务实现类
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@Service
public class InformationServiceImpl extends ServiceImpl<InformationMapper, Information> implements InformationService {

}
