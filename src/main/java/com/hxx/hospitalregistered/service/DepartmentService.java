package com.hxx.hospitalregistered.service;

import com.hxx.hospitalregistered.model.Department;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 科室 服务类
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
public interface DepartmentService extends IService<Department> {

}
