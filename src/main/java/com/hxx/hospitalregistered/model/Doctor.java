package com.hxx.hospitalregistered.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 医生表
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@TableName("t_doctor")
public class Doctor implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 姓名
     */
    private String docName;

    /**
     * 性别
     */
    private String docSex;

    /**
     * 电话
     */
    private String docTel;

    /**
     * 出生年月
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date docDate;

    /**
     * 职称
     */
    private String docTitle;

    /**
     * 密码
     */
    private String docPassword;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getDocSex() {
        return docSex;
    }

    public void setDocSex(String docSex) {
        this.docSex = docSex;
    }

    public String getDocTel() {
        return docTel;
    }

    public void setDocTel(String docTel) {
        this.docTel = docTel;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Date getDocDate() {
        return docDate;
    }

    public void setDocDate(Date docDate) {
        this.docDate = docDate;
    }

    public String getDocTitle() {
        return docTitle;
    }

    public void setDocTitle(String docTitle) {
        this.docTitle = docTitle;
    }

    public String getDocPassword() {
        return docPassword;
    }

    public void setDocPassword(String docPassword) {
        this.docPassword = docPassword;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", docName=" + docName +
                ", docSex=" + docSex +
                ", docTel=" + docTel +
                ", docDate=" + docDate +
                ", docTitle=" + docTitle +
                ", docPassword=" + docPassword +
                "}";
    }
}
