package com.hxx.hospitalregistered.controller;


import com.hxx.hospitalregistered.model.Doctor;
import com.hxx.hospitalregistered.service.DoctorService;
import com.hxx.hospitalregistered.utils.ResponseOBJ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 医生表 前端控制器
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@RestController
@RequestMapping("/doctor")
public class DoctorController {

    @Autowired
    DoctorService doctorService;

    /**
     * 查询所有的医生，分页的在admin里面
     *
     * @return
     */
    @GetMapping("/listAllDoctor")
    public ResponseOBJ<List<Doctor>> listAllDoctor() {
        ResponseOBJ<List<Doctor>> responseOBJ = ResponseOBJ.getInstance();
        List<Doctor> list = doctorService.list();
        responseOBJ.setData(list);
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }

}

