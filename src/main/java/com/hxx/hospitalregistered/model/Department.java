package com.hxx.hospitalregistered.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * 科室
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@TableName("t_department")
public class Department implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 医生编号
     */
    private Integer doctorNo;

    /**
     * 科室地址
     */
    private String depAddress;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDoctorNo() {
        return doctorNo;
    }

    public void setDoctorNo(Integer doctorNo) {
        this.doctorNo = doctorNo;
    }

    public String getDepAddress() {
        return depAddress;
    }

    public void setDepAddress(String depAddress) {
        this.depAddress = depAddress;
    }

    @Override
    public String toString() {
        return "Department{" +
        "id=" + id +
        ", doctorNo=" + doctorNo +
        ", depAddress=" + depAddress +
        "}";
    }
}
