package com.hxx.hospitalregistered.service;

import com.hxx.hospitalregistered.model.Department;
import com.hxx.hospitalregistered.mapper.DepartmentMapper;
import com.hxx.hospitalregistered.service.DepartmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 科室 服务实现类
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, Department> implements DepartmentService {

}
