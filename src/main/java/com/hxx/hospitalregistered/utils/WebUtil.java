package com.hxx.hospitalregistered.utils;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @Author:
 * @Date:2020/2/24
 * @Explain:weekly web工具类
 */
public class WebUtil {


    /**
     * 拿到request域对象,解耦方式获取
     * @return
     */
    public static HttpServletRequest getRequest(){
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        return request;
    }

    /**
     * 拿到session域对象
     */
    public static HttpSession getSession(){
        return getRequest().getSession();
    }
}
