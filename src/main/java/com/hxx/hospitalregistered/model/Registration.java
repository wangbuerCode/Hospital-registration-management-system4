package com.hxx.hospitalregistered.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 挂号表
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@TableName("t_registration")
public class Registration implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 医生编号
     */
    private Integer doctorNo;

    /**
     * 科室编号
     */
    private Integer departmentNo;

    /**
     * 患者姓名
     */
    private String patName;

    /**
     * 挂号时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date regTime;

    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDoctorNo() {
        return doctorNo;
    }

    public void setDoctorNo(Integer doctorNo) {
        this.doctorNo = doctorNo;
    }

    public Integer getDepartmentNo() {
        return departmentNo;
    }

    public void setDepartmentNo(Integer departmentNo) {
        this.departmentNo = departmentNo;
    }

    public String getPatName() {
        return patName;
    }

    public void setPatName(String patName) {
        this.patName = patName;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Date getRegTime() {
        return regTime;
    }

    public void setRegTime(Date regTime) {
        this.regTime = regTime;
    }

    @Override
    public String toString() {
        return "Registration{" +
                "id=" + id +
                ", doctorNo=" + doctorNo +
                ", departmentNo=" + departmentNo +
                ", patName='" + patName + '\'' +
                ", regTime=" + regTime +
                ", date='" + date + '\'' +
                '}';
    }
}
