package com.hxx.hospitalregistered.service;

import com.hxx.hospitalregistered.model.Registration;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 挂号表 服务类
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
public interface RegistrationService extends IService<Registration> {

}
