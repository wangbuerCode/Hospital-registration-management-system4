package com.hxx.hospitalregistered.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 患者表
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@TableName("t_patient")
public class Patient implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 患者病号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 患者姓名
     */
    private String patName;

    /**
     * 患者性别
     */
    private String patSex;

    /**
     * 患者电话

     */
    private String patTel;

    /**
     * 出生年月
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date patDate;

    /**
     * 患者地址
     */
    private String patAddress;

    /**
     * 密码
     */
    private String patPassword;

    /**
     * 身份证号码
     */
    private String patId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPatName() {
        return patName;
    }

    public void setPatName(String patName) {
        this.patName = patName;
    }

    public String getPatSex() {
        return patSex;
    }

    public void setPatSex(String patSex) {
        this.patSex = patSex;
    }

    public String getPatTel() {
        return patTel;
    }

    public void setPatTel(String patTel) {
        this.patTel = patTel;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Date getPatDate() {
        return patDate;
    }

    public void setPatDate(Date patDate) {
        this.patDate = patDate;
    }

    public String getPatAddress() {
        return patAddress;
    }

    public void setPatAddress(String patAddress) {
        this.patAddress = patAddress;
    }

    public String getPatPassword() {
        return patPassword;
    }

    public void setPatPassword(String patPassword) {
        this.patPassword = patPassword;
    }

    public String getPatId() {
        return patId;
    }

    public void setPatId(String patId) {
        this.patId = patId;
    }

    @Override
    public String toString() {
        return "Patient{" +
        "id=" + id +
        ", patName=" + patName +
        ", patSex=" + patSex +
        ", patTel=" + patTel +
        ", patDate=" + patDate +
        ", patAddress=" + patAddress +
        ", patPassword=" + patPassword +
        ", patId=" + patId +
        "}";
    }
}
