package com.hxx.hospitalregistered.service;

import com.hxx.hospitalregistered.model.Admin;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 管理员表 服务类
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
public interface AdminService extends IService<Admin> {

}
