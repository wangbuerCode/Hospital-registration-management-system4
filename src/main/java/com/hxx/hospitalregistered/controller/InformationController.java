package com.hxx.hospitalregistered.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hxx.hospitalregistered.model.Information;
import com.hxx.hospitalregistered.service.InformationService;
import com.hxx.hospitalregistered.utils.ResponseOBJ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 咨询表 前端控制器
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@RestController
@RequestMapping("/information")
public class InformationController {

    @Autowired
    InformationService informationService;

    @PostMapping("/addInformation")
    public ResponseOBJ<Boolean> addInformation(@RequestBody Information information) {
        ResponseOBJ<Boolean> responseOBJ = ResponseOBJ.getInstance();
        information.setInfTime(new Date());
        boolean save = informationService.save(information);
        responseOBJ.setData(save);
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }

    @PostMapping("/deleteInformation")
    public ResponseOBJ<Boolean> deleteInformation(@RequestParam("id") Integer id) {
        ResponseOBJ<Boolean> responseOBJ = ResponseOBJ.getInstance();
        boolean remove = informationService.removeById(id);
        responseOBJ.setData(remove);
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }

    @GetMapping("/list")
    public ResponseOBJ<List<Information>> list() {
        ResponseOBJ<List<Information>> responseOBJ = ResponseOBJ.getInstance();
        List<Information> information = informationService.list(new QueryWrapper<Information>().orderByDesc("inf_time"));
        responseOBJ.setData(information);
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }

    @PostMapping("/update")
    public ResponseOBJ<Boolean> update(@RequestBody Information information) {
        ResponseOBJ<Boolean> responseOBJ = ResponseOBJ.getInstance();
        boolean update = informationService.updateById(information);
        responseOBJ.setData(update);
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }
}

