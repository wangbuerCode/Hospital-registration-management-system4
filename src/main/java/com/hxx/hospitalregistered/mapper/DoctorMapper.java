package com.hxx.hospitalregistered.mapper;

import com.hxx.hospitalregistered.model.Doctor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 医生表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
public interface DoctorMapper extends BaseMapper<Doctor> {

}
