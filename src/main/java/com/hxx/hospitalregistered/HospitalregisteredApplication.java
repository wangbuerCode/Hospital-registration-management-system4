package com.hxx.hospitalregistered;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.hxx.hospitalregistered.mapper")
public class HospitalregisteredApplication {

    public static void main(String[] args) {
        SpringApplication.run(HospitalregisteredApplication.class, args);
    }

}
