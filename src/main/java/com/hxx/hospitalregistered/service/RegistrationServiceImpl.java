package com.hxx.hospitalregistered.service;

import com.hxx.hospitalregistered.model.Registration;
import com.hxx.hospitalregistered.mapper.RegistrationMapper;
import com.hxx.hospitalregistered.service.RegistrationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 挂号表 服务实现类
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@Service
public class RegistrationServiceImpl extends ServiceImpl<RegistrationMapper, Registration> implements RegistrationService {

}
