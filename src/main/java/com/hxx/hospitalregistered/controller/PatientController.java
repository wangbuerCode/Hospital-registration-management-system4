package com.hxx.hospitalregistered.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hxx.hospitalregistered.model.Patient;
import com.hxx.hospitalregistered.service.PatientService;
import com.hxx.hospitalregistered.utils.ResponseOBJ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 患者表 前端控制器
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@RestController
@RequestMapping("/patient")
public class PatientController {

    @Autowired
    PatientService patientService;

    /**
     * 查询所有的患者，分页的在admin里面
     *
     * @return
     */
    @GetMapping("/listAllPatient")
    public ResponseOBJ<List<Patient>> listAllPatient() {
        ResponseOBJ<List<Patient>> responseOBJ = ResponseOBJ.getInstance();
        List<Patient> list = patientService.list();
        responseOBJ.setData(list);
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }


    /**
     * 根据姓名查询患者
     * @param patName
     * @return
     */
    @GetMapping("/getPatientByName")
    public ResponseOBJ<Patient> getPatientByName(@RequestParam("patName") String patName){
        ResponseOBJ<Patient> responseOBJ = ResponseOBJ.getInstance();
        Patient p = new Patient();
        p.setPatName(patName);
        responseOBJ.setData(patientService.getOne(new QueryWrapper<Patient>(p)));
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }





    /**
     * 当点击下一个的时候，传当前患者ID过来
     * @param id
     * @return
     */
    @GetMapping("/nextPatient")
    public ResponseOBJ<Patient> nextPatient(@RequestParam("id") Integer id) {
        ResponseOBJ<Patient> responseOBJ = ResponseOBJ.getInstance();
        Patient patient = patientService.getById(id + 1);
        responseOBJ.setData(patient);
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }

}

