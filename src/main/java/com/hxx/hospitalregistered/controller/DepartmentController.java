package com.hxx.hospitalregistered.controller;


import com.hxx.hospitalregistered.model.Department;
import com.hxx.hospitalregistered.service.DepartmentService;
import com.hxx.hospitalregistered.utils.ResponseOBJ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 科室 前端控制器
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@RestController
@RequestMapping("/department")
public class DepartmentController {
    @Autowired
    DepartmentService departmentService;

    /**
     * 查询所有的科室
     */
    @GetMapping("/listAllDept")
    public ResponseOBJ<List<Department>> listAllDept() {
        ResponseOBJ<List<Department>> responseOBJ = ResponseOBJ.getInstance();
        List<Department> list = departmentService.list();
        responseOBJ.setData(list);
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }
}

