package com.hxx.hospitalregistered.service;

import com.hxx.hospitalregistered.model.Patient;
import com.hxx.hospitalregistered.mapper.PatientMapper;
import com.hxx.hospitalregistered.service.PatientService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 患者表 服务实现类
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@Service
public class PatientServiceImpl extends ServiceImpl<PatientMapper, Patient> implements PatientService {

}
