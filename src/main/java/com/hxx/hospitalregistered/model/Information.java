package com.hxx.hospitalregistered.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 咨询表
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@TableName("t_Information")
public class Information implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 资讯内容
     */
    private String infContext;

    /**
     * 资讯标题
     */
    private String infTitle;

    /**
     * 发布时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date infTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInfContext() {
        return infContext;
    }

    public void setInfContext(String infContext) {
        this.infContext = infContext;
    }

    public String getInfTitle() {
        return infTitle;
    }

    public void setInfTitle(String infTitle) {
        this.infTitle = infTitle;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Date getInfTime() {
        return infTime;
    }

    public void setInfTime(Date infTime) {
        this.infTime = infTime;
    }

    @Override
    public String toString() {
        return "Information{" +
                "id=" + id +
                ", infContext=" + infContext +
                ", infTitle=" + infTitle +
                ", infTime=" + infTime +
                "}";
    }
}
