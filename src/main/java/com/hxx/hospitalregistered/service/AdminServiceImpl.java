package com.hxx.hospitalregistered.service;

import com.hxx.hospitalregistered.model.Admin;
import com.hxx.hospitalregistered.mapper.AdminMapper;
import com.hxx.hospitalregistered.service.AdminService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 管理员表 服务实现类
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {

}
