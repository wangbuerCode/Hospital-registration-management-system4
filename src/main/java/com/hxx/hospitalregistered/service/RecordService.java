package com.hxx.hospitalregistered.service;

import com.hxx.hospitalregistered.model.Record;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 病例 服务类
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
public interface RecordService extends IService<Record> {

}
