package com.hxx.hospitalregistered.service;

import com.hxx.hospitalregistered.model.Result;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 结果 服务类
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
public interface ResultService extends IService<Result> {

}
