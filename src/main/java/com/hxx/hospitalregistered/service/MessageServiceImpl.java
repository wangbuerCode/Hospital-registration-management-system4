package com.hxx.hospitalregistered.service;

import com.hxx.hospitalregistered.model.Message;
import com.hxx.hospitalregistered.mapper.MessageMapper;
import com.hxx.hospitalregistered.service.MessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements MessageService {

}
