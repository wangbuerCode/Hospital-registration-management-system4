package com.hxx.hospitalregistered.controller;

import com.hxx.hospitalregistered.model.Admin;
import com.hxx.hospitalregistered.model.Doctor;
import com.hxx.hospitalregistered.model.Patient;
import com.hxx.hospitalregistered.utils.WebUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author:蒋凌鹏
 * @Date:2020/3/11
 * @Explain:hospitalregistered
 */
@Controller
@RequestMapping("/sys")
public class SystemController {


    /**
     * 跳转到登录页
     * @return
     */
    @GetMapping("/login")
    public String toLogin(){
        return "login";
    }


    /**
     * 退出系统
     * @return
     */
    @GetMapping("/signOut")
    public String signOut(){
        WebUtil.getSession().setAttribute("admin",null);
        WebUtil.getSession().setAttribute("doctor",null);
        WebUtil.getSession().setAttribute("patient",null);
        return "login";
    }



    /**
     * 跳转到管理员首页
     * @return
     */
    @GetMapping("/toAdmin")
    public String toAdmin(){
        Admin admin = (Admin) WebUtil.getSession().getAttribute("admin");
        return admin != null ? "admin/index" : "login";
    }


    /**
     * 跳转到医生首页
     * @return
     */
    @GetMapping("/toDoctor")
    public String toDoctor(){
        Doctor doctor = (Doctor) WebUtil.getSession().getAttribute("doctor");
        return doctor != null ? "doctor/index" : "login";
    }


    /**
     * 跳转到患者首页
     * @return
     */
    @GetMapping("/toPatient")
    public String toPatient(){
        Patient patient = (Patient) WebUtil.getSession().getAttribute("patient");
        return patient != null ? "patient/index" : "login";
    }




}
