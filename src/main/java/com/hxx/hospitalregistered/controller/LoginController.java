package com.hxx.hospitalregistered.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hxx.hospitalregistered.model.Admin;
import com.hxx.hospitalregistered.model.Doctor;
import com.hxx.hospitalregistered.model.Patient;
import com.hxx.hospitalregistered.service.AdminService;
import com.hxx.hospitalregistered.service.DoctorService;
import com.hxx.hospitalregistered.service.PatientService;
import com.hxx.hospitalregistered.utils.ResponseOBJ;
import com.hxx.hospitalregistered.utils.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 
 * @2020-03-11 21:38
 */
@RestController
@RequestMapping("/loginController")
public class LoginController {

    @Autowired
    private AdminService adminService;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private PatientService patientService;


    @PostMapping("/login")
    public ResponseOBJ<Map<String, Object>> login(@RequestParam("identity") String identity,
                      @RequestParam("username") String username,
                      @RequestParam("password") String password) {
        ResponseOBJ<Map<String, Object>> responseOBJ = ResponseOBJ.getInstance();
        HttpSession session = WebUtil.getSession();
        Map<String, Object> map = new HashMap<>();
        if (identity.equals("admin")) {
            Admin admin = new Admin();
            admin.setAdName(username);
            admin.setAdPassword(password);
            Admin adminServiceOne = adminService.getOne(new QueryWrapper<Admin>(admin));
            map.put("identity", "admin");
            map.put("admin", adminServiceOne);
            session.setAttribute("admin", adminServiceOne);
        } else if (identity.equals("doctor")) {
            Doctor doctor = new Doctor();
            doctor.setDocName(username);
            doctor.setDocPassword(password);
            Doctor doctorServiceOne = doctorService.getOne(new QueryWrapper<Doctor>(doctor));
            map.put("identity", "doctor");
            map.put("doctor", doctorServiceOne);
            session.setAttribute("doctor", doctorServiceOne);
        } else if (identity.equals("patient")) {
            Patient patient = new Patient();
            patient.setPatName(username);
            patient.setPatPassword(password);
            Patient patientServiceOne = patientService.getOne(new QueryWrapper<Patient>(patient));
            map.put("identity", "patient");
            map.put("patient", patientServiceOne);
            session.setAttribute("patient", patientServiceOne);
        }
        responseOBJ.setData(map);
        return responseOBJ;
    }

}
