package com.hxx.hospitalregistered.service;

import com.hxx.hospitalregistered.model.Information;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 咨询表 服务类
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
public interface InformationService extends IService<Information> {

}
