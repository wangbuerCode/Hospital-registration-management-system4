package com.hxx.hospitalregistered.service;

import com.hxx.hospitalregistered.model.Doctor;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 医生表 服务类
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
public interface DoctorService extends IService<Doctor> {

}
