package com.hxx.hospitalregistered.mapper;

import com.hxx.hospitalregistered.model.Patient;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 患者表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
public interface PatientMapper extends BaseMapper<Patient> {

}
