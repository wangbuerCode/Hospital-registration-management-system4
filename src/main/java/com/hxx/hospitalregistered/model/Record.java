package com.hxx.hospitalregistered.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 病例
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@TableName("t_record")
public class Record implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 患者编号
     */
    private Integer patientNo;

    /**
     * 医生编号
     */
    private Integer doctorNo;

    /**
     * 诊断结果
     */
    private String recContext;

    /**
     * 诊断时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date recTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPatientNo() {
        return patientNo;
    }

    public void setPatientNo(Integer patientNo) {
        this.patientNo = patientNo;
    }

    public Integer getDoctorNo() {
        return doctorNo;
    }

    public void setDoctorNo(Integer doctorNo) {
        this.doctorNo = doctorNo;
    }

    public String getRecContext() {
        return recContext;
    }

    public void setRecContext(String recContext) {
        this.recContext = recContext;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Date getRecTime() {
        return recTime;
    }

    public void setRecTime(Date recTime) {
        this.recTime = recTime;
    }

    @Override
    public String toString() {
        return "Record{" +
        "id=" + id +
        ", patientNo=" + patientNo +
        ", doctorNo=" + doctorNo +
        ", recContext=" + recContext +
        ", recTime=" + recTime +
        "}";
    }
}
