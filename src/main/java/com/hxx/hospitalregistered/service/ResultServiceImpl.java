package com.hxx.hospitalregistered.service;

import com.hxx.hospitalregistered.model.Result;
import com.hxx.hospitalregistered.mapper.ResultMapper;
import com.hxx.hospitalregistered.service.ResultService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 结果 服务实现类
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@Service
public class ResultServiceImpl extends ServiceImpl<ResultMapper, Result> implements ResultService {

}
