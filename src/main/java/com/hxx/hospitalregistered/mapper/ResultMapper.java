package com.hxx.hospitalregistered.mapper;

import com.hxx.hospitalregistered.model.Result;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 结果 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
public interface ResultMapper extends BaseMapper<Result> {

}
