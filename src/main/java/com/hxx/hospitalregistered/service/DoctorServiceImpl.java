package com.hxx.hospitalregistered.service;

import com.hxx.hospitalregistered.model.Doctor;
import com.hxx.hospitalregistered.mapper.DoctorMapper;
import com.hxx.hospitalregistered.service.DoctorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 医生表 服务实现类
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@Service
public class DoctorServiceImpl extends ServiceImpl<DoctorMapper, Doctor> implements DoctorService {

}
