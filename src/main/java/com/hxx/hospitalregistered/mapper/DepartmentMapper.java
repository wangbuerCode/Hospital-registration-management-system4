package com.hxx.hospitalregistered.mapper;

import com.hxx.hospitalregistered.model.Department;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 科室 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
public interface DepartmentMapper extends BaseMapper<Department> {

}
