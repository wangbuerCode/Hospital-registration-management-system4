package com.hxx.hospitalregistered.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hxx.hospitalregistered.model.Patient;
import com.hxx.hospitalregistered.model.Result;
import com.hxx.hospitalregistered.service.ResultService;
import com.hxx.hospitalregistered.utils.ResponseOBJ;
import com.hxx.hospitalregistered.utils.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 结果 前端控制器
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@RestController
@RequestMapping("/result")
public class ResultController {

    @Autowired
    ResultService resultService;

    /**
     * 结果医生写，医生和患者都可以看（这是需求）
     */

    /**
     * patientNo:患者病号下拉框选
     */
    @PostMapping("/addResult")
    public ResponseOBJ<Boolean> addResult(@RequestBody Result record) {
        ResponseOBJ<Boolean> responseOBJ = ResponseOBJ.getInstance();
        boolean save = resultService.save(record);
        responseOBJ.setData(save);
        responseOBJ.setCode(ResponseOBJ.SUCCESS);
        return responseOBJ;
    }


    @GetMapping("/listResult")
    public ResponseOBJ<List<Result>> listResult(@RequestParam("page") Integer page,
                                                @RequestParam("limit") Integer limit) {
        ResponseOBJ<List<Result>> responseOBJ = ResponseOBJ.getInstance();
        Patient patient = (Patient) WebUtil.getSession().getAttribute("patient");
        /**
         * 患者只能看见自己的检查结果
         */
        if (patient.getId() != null) {
            Result result = new Result();
            result.setPatientNo(patient.getId());
            PageHelper.startPage(page, limit);
            List<Result> list = resultService.list(new QueryWrapper<Result>(result));
            PageInfo<Result> pageInfo = new PageInfo<>(list);
            responseOBJ.setData(list);
            responseOBJ.setCount(pageInfo.getTotal());
            responseOBJ.setCode(ResponseOBJ.SUCCESS);
        } else {
            PageHelper.startPage(page, limit);
            List<Result> results = resultService.list();
            PageInfo<Result> pageInfo = new PageInfo<>(results);
            responseOBJ.setData(results);
            responseOBJ.setCount(pageInfo.getTotal());
            responseOBJ.setCode(ResponseOBJ.SUCCESS);
        }
        return responseOBJ;
    }

}

