package com.hxx.hospitalregistered.service;

import com.hxx.hospitalregistered.model.Message;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
public interface MessageService extends IService<Message> {

}
