package com.hxx.hospitalregistered.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * 结果
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@TableName("t_result")
public class Result implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 患者病号
     */
    private Integer patientNo;

    /**
     * 结果内容
     */
    private String resContext;

    /**
     * 检查名称
     */
    private String resName;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPatientNo() {
        return patientNo;
    }

    public void setPatientNo(Integer patientNo) {
        this.patientNo = patientNo;
    }

    public String getResContext() {
        return resContext;
    }

    public void setResContext(String resContext) {
        this.resContext = resContext;
    }

    public String getResName() {
        return resName;
    }

    public void setResName(String resName) {
        this.resName = resName;
    }

    @Override
    public String toString() {
        return "Result{" +
        "id=" + id +
        ", patientNo=" + patientNo +
        ", resContext=" + resContext +
        ", resName=" + resName +
        "}";
    }
}
