package com.hxx.hospitalregistered.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@TableName("t_message")
public class Message implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 患者病号
     */
    private Integer patientNo;

    /**
     * 医生号
     */
    private Integer doctorNo;

    private String mesContext;

    /**
     * 留言时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date mesTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPatientNo() {
        return patientNo;
    }

    public void setPatientNo(Integer patientNo) {
        this.patientNo = patientNo;
    }

    public Integer getDoctorNo() {
        return doctorNo;
    }

    public void setDoctorNo(Integer doctorNo) {
        this.doctorNo = doctorNo;
    }

    public String getMesContext() {
        return mesContext;
    }

    public void setMesContext(String mesContext) {
        this.mesContext = mesContext;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Date getMesTime() {
        return mesTime;
    }

    public void setMesTime(Date mesTime) {
        this.mesTime = mesTime;
    }

    @Override
    public String toString() {
        return "Message{" +
        "id=" + id +
        ", patientNo=" + patientNo +
        ", doctorNo=" + doctorNo +
        ", mesContext=" + mesContext +
        ", mesTime=" + mesTime +
        "}";
    }
}
