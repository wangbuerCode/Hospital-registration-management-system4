package com.hxx.hospitalregistered.service;

import com.hxx.hospitalregistered.model.Record;
import com.hxx.hospitalregistered.mapper.RecordMapper;
import com.hxx.hospitalregistered.service.RecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 病例 服务实现类
 * </p>
 *
 * @author 
 * @since 2020-03-11
 */
@Service
public class RecordServiceImpl extends ServiceImpl<RecordMapper, Record> implements RecordService {

}
